﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	public static int score = 0;
	private Text myText;
	// Use this for initialization
	void Start () {
		//Define myText as my own Text object
		myText = gameObject.GetComponent<Text>();
		Reset();
	}
	
	public void ScorePoints(int points){
		score += points;
		myText.text = score.ToString();
	}
	
	public static void Reset(){
		score = 0;
		//myText.text = score.ToString();
	}
}
