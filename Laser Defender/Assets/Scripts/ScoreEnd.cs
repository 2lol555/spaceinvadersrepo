﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreEnd : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Text myText = GetComponent<Text>();
		myText.text = "Score: " + Score.score.ToString();
		Score.Reset();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
