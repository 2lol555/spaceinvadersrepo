﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public float shipSpeed = 5f;
	public float padding = 0.7f;
	public float projectileSpeed = 10f;
	public GameObject laserProjectile;
	public float firingRate = 0.2f;
	public float health = 250f;
	public AudioClip fireSound;
	public AudioClip hitSound;
	
	float xmin;
	float xmax;
	
	void Start () {
		float distance = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
		Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
		xmin = leftmost.x + padding;
		xmax = rightmost.x - padding;
	}
	
	void Update(){
		HandleInput();
	}
	
	void HandleInput(){
		if(Input.GetKey(KeyCode.LeftArrow)){
			transform.position += Vector3.left * shipSpeed * Time.deltaTime;
		}else if(Input.GetKey(KeyCode.RightArrow)){
			transform.position += Vector3.right * shipSpeed * Time.deltaTime;
		}
		if(Input.GetKeyDown(KeyCode.Space)){
			InvokeRepeating("Fire", 0.05F, firingRate);
		}
		if (Input.GetKeyUp(KeyCode.Space)){
			CancelInvoke("Fire");
		}
		//Restricting movement on x axis to game space
		float newX = Mathf.Clamp(transform.position.x, xmin, xmax);
		transform.position = new Vector3(newX, transform.position.y, transform.position.z);
	}
	
	void Fire(){
		AudioSource.PlayClipAtPoint(fireSound, transform.position);
		Vector3 offset = new Vector3(0, 0.7f);
		GameObject beam = Instantiate(laserProjectile, transform.position + offset, Quaternion.identity) as GameObject;
		beam.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, projectileSpeed);
	}
	
	void OnTriggerEnter2D(Collider2D beam){
		//Find if the collider has a "Projectile component attached"
		Projectile missile = beam.gameObject.GetComponent<Projectile>();
		if(missile){
			AudioSource.PlayClipAtPoint(hitSound, transform.position);
			print("Projectile has hit the player");
			health -= missile.GetDamage();
			missile.hit();
			if(health <= 0){
				LevelManager manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
				manager.LoadLevel("Win_Screen");
			}
		}
	}
}
