﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {
	public AudioClip startClip;
	public AudioClip gameClip;
	public AudioClip endClip;
	
	private AudioSource audioSource;

	static MusicPlayer instance; 
	// Use this for initialization
	void Start () {
		if(instance == null){
			audioSource = GetComponent<AudioSource>();
			GameObject.DontDestroyOnLoad(gameObject);
			instance = this;
			GetComponent<AudioSource>().Play();
			
		} else {
			Destroy(gameObject);
		}
	}
	void OnLevelWasLoaded(int level){
		Debug.Log("Music player loaded for level " + level);
		audioSource.Stop();
		
		if (level == 0){audioSource.clip = startClip;
		}else if (level == 1){audioSource.clip = gameClip;
		}else if (level == 2){audioSource.clip = endClip;
		}
		audioSource.Play();
	}
}