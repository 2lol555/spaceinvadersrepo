﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public float health = 100f;
	public GameObject laserProjectile;
	public float projectileSpeed;
	public float shotsPerSecond = 0.5f;
	public int scoreValue = 150;
	public AudioClip fireSound;
	public AudioClip dieSound;
	
	private Score score;
	
	void Start(){
		//Find an object named Score and get its Score component (In our case the script)
		score = GameObject.Find("Score").GetComponent<Score>();
		
	}

	void Update(){
		float probability = shotsPerSecond * Time.deltaTime;
		if(Random.value < probability){
			Fire();
		}
	}
	void OnTriggerEnter2D(Collider2D beam){
		//Find if the object that colided with us has a "Projectile component attached"
		Projectile missile = beam.gameObject.GetComponent<Projectile>();
		//If it exists then ...
		if(missile){
			print("Projectile has hit a ship");
			health -= missile.GetDamage();
			missile.hit();
			if(health <= 0){
				//Call the ScorePoints method in the score instance
				score.ScorePoints(scoreValue);
				AudioSource.PlayClipAtPoint(dieSound, transform.position);
				Destroy(gameObject);		
			}
		}
	}
	
	void Fire(){
		AudioSource.PlayClipAtPoint(fireSound, transform.position);
		Vector3 projectileStart = transform.position + new Vector3(0f, -0.7f);
		GameObject beam = Instantiate(laserProjectile, projectileStart, Quaternion.identity) as GameObject;
		beam.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, -projectileSpeed);
	}
}
