﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
		private bool movingLeft = true;
		float xmin;
		float xmax;
		private int enemyCount;

		public float alienSpeed = 10f;
		public GameObject enemyPrefab;
		public float width = 10f;
		public float height = 5f;
		public float spawnDelay = 0.25f;

	void Start () {
		//In use for 3d games
		float distance = transform.position.z - Camera.main.transform.position.z;
		//Taking note of the leftmost and rightmost point of the play area
		Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
		Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
		//setting the minimum and maximum x positions
		xmin = leftmost.x + (width / 2);
		xmax = rightmost.x - (width / 2);
		SpawnUntillFull();
	}
	
	void SpawnUntillFull(){
		Transform nextPos = NextFreePosition();
		if(nextPos){
			//for each of my child entities spawn an alien as their child
			GameObject enemy = Instantiate(enemyPrefab, nextPos.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = nextPos;
			Invoke("SpawnUntillFull", spawnDelay);
		}
	}
	
	//Drawing a bounding box of the formation
	public void OnDrawGizmos(){Gizmos.DrawWireCube(transform.position, new Vector3(width,height));}
	
	void Update () {
	
		//Moving the formation
		if (movingLeft) {transform.position += Vector3.left * Time.deltaTime * alienSpeed;}
		else {transform.position += Vector3.right * Time.deltaTime * alienSpeed;}
		
		//checking if formation is on either edge if true negating movement value
		if(transform.position.x >= xmax || transform.position.x <= xmin){movingLeft = !movingLeft;}
		
		//Restrict the movement to the play field (Just in case)
		float newX = Mathf.Clamp(transform.position.x, xmin, xmax);
		transform.position = new Vector3(newX, transform.position.y, transform.position.z);
		if(AllMembersDead()){
			Debug.Log("Empty formation");
			SpawnUntillFull();
		}
	}
	
	Transform NextFreePosition(){
		foreach(Transform childPositionGameObject in transform){
			if(childPositionGameObject.childCount == 0){
				return childPositionGameObject.transform;	
			}
		}
		Debug.Log("Returning null");
		return null;

	}
	
	bool AllMembersDead(){
		//Scan all children in this object, if there is any child under them return false otherwise true.
		foreach(Transform childPositionGameObject in transform){
			if(childPositionGameObject.childCount > 0){
				return false;	
			}
		}
		return true;
	}
}

